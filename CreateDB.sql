-- Script de création de la base et des utilisateurs
DROP DATABASE IF EXISTS Airport;

CREATE DATABASE Airport;

DROP USER IF EXISTS 'admin'@'localhost';

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'adminpasswd';

GRANT ALL PRIVILEGES ON Airport.* TO 'admin'@'localhost'
WITH GRANT OPTION;

DROP USER IF EXISTS 'user'@'localhost';

CREATE USER 'user'@'localhost' IDENTIFIED BY 'userpasswd';

GRANT SELECT ON Airport.* TO 'user'@'localhost';
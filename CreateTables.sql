-- Script de création des tables

use Airport;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS Company;
DROP TABLE IF EXISTS Runway;
DROP TABLE IF EXISTS Employe;
DROP TABLE IF EXISTS Airport;
DROP TABLE IF EXISTS Distance;
DROP TABLE IF EXISTS Flight;
DROP TABLE IF EXISTS Passenger;
DROP TABLE IF EXISTS FlightPlan;
DROP TABLE IF EXISTS Plane;
DROP TABLE IF EXISTS PlaneType;
DROP TABLE IF EXISTS Country;
DROP TABLE IF EXISTS Continent;
DROP TABLE IF EXISTS FlightEmploye;


CREATE TABLE Continent (
codeContinent	VARCHAR(3)	NOT NULL,
Name			VARCHAR(25),

PRIMARY KEY (codeContinent)
);


CREATE TABLE Country (
numCountry		INTEGER		NOT NULL,
codeCountry		VARCHAR(3)	NOT NULL,
Name			VARCHAR(50),
codeContinent	VARCHAR(3)	NOT NULL,

PRIMARY KEY (codeCountry),
FOREIGN KEY (codeContinent)
	REFERENCES Continent(codeContinent)
);


CREATE TABLE Company (
numCompany	INTEGER 	NOT NULL,
Name		VARCHAR(90),
IATA		VARCHAR(5),

PRIMARY KEY (numCompany)
);

CREATE TABLE Employe (
numEmploye	INTEGER		NOT NULL,
Name		VARCHAR(20),
Age			INTEGER,
Address		VARCHAR(40),
Salary		INTEGER,
Service		VARCHAR(30),
AirportIATA	VARCHAR(5)	NOT NULL,
Job			VARCHAR(30),

PRIMARY KEY (numEmploye),
FOREIGN KEY (AirportIATA)
	REFERENCES Airport(AirportIATA)
);


CREATE TABLE FlightEmploye (
numEmploye 	INTEGER		NOT NULL,
numFlight	INTEGER 	NOT NULL,
PRIMARY KEY (numEmploye, numFlight),
FOREIGN KEY (numEmploye)
	REFERENCES Employe(numEmploye) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (numFlight)
	REFERENCES Flight(numFlight) ON DELETE CASCADE ON UPDATE CASCADE

);


CREATE TABLE Airport (
AirportIATA	VARCHAR(5)	NOT NULL,
AirportOACI	VARCHAR(10) ,
Name		VARCHAR(100),
Creation	DATETIME,
Latitude	FLOAT,
Longitude	FLOAT,
Altitude	FLOAT,
Type		VARCHAR(20),
codeCountry	VARCHAR(3)		NOT NULL,

PRIMARY KEY (AirportIATA,AirportOACI),
FOREIGN KEY (codeCountry)
	REFERENCES Country(codeCountry),
	INDEX id_AirportOACI (AirportOACI)
);


CREATE TABLE Distance (
Airport1OACI 	VARCHAR(10) NOT NULL,
Airport2OACI 	VARCHAR(10) NOT NULL,
Distance 		FLOAT,
PRIMARY KEY (Airport1OACI,Airport2OACI),
FOREIGN KEY (Airport1OACI)
	REFERENCES Airport(AirportOACI),
FOREIGN KEY (Airport2OACI)
	REFERENCES Airport(AirportOACI)

);

CREATE TABLE Runway (
numRunway	INTEGER		NOT NULL,
Length		INTEGER,
LastCheckup	DATETIME,
Open		BOOLEAN,
Surface		VARCHAR(70),
AirportOACI	VARCHAR(10)  NULL,

PRIMARY KEY (numRunway),
FOREIGN KEY (AirportOACI)
	REFERENCES Airport(AirportOACI)
);


CREATE TABLE Flight (
numFlight	INTEGER NOT NULL,
PlaneIATA 	VARCHAR(5),
numCompany	INTEGER,

PRIMARY KEY (numFlight),
FOREIGN KEY (PlaneIATA)
	REFERENCES Plane(IATA),
FOREIGN KEY (numCompany)
	REFERENCES Company(numCompany)
);


CREATE TABLE Passenger (
numPassenger	INTEGER 	NOT NULL,
Name 			VARCHAR(50) NOT NULL,
Age				INTEGER,
Address			VARCHAR(100),
numFlight		INTEGER,

PRIMARY KEY (numPassenger),
FOREIGN KEY (numFlight)
	REFERENCES Flight(numFlight)
);


CREATE TABLE Plane (
IATA		VARCHAR(5)	NOT NULL,
Maker		VARCHAR(30),
Model		VARCHAR(50),
Capacity	INTEGER,
Autonomy	INTEGER,

PRIMARY KEY (IATA)
);



CREATE TABLE FlightPlan (
idFlight	INTEGER		NOT NULL AUTO_INCREMENT,
numFlight 	INTEGER 	NOT NULL,
Airport 	VARCHAR(5) 	NOT NULL,
Departure	datetime,
Arrival		datetime,

PRIMARY KEY (idFlight),
FOREIGN KEY (numFlight)
	REFERENCES Flight(numFlight),
FOREIGN KEY (Airport)
	REFERENCES Airport(AirportIATA)
);

SET FOREIGN_KEY_CHECKS=1;

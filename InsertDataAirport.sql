


use Airport;

LOAD DATA LOCAL INFILE "airports.csv"
  REPLACE
  INTO TABLE Airport
  FIELDS
	OPTIONALLY ENCLOSED BY '"'
	TERMINATED BY ','
  LINES
	TERMINATED BY '\r\n'
  IGNORE 1 LINES
  (@id,@ident,@type,Name,@Latitude,@Longitude,@Altitude,@continent,codeCountry,@iso_region,@municipality,@scheduled_service,@gps_code,AirportIATA,@local_code,@home_link,@wikipedia_link,@keywords)
  SET
  AirportOACI = @ident,
  Latitude = case @Latitude when '' then null else @Latitude END,
  Longitude = case @Longitude when '' then null else @Longitude END,
  Altitude = case @Altitude when '' then null else @Altitude END
  ;




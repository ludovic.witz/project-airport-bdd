use Airport;

LOAD DATA LOCAL INFILE "company.csv"
  REPLACE
  INTO TABLE Company
  FIELDS
	OPTIONALLY ENCLOSED BY '"'
	TERMINATED BY ','
  LINES
	TERMINATED BY '\r\n'
  IGNORE 1 LINES
  (numCompany, Name, @dummy, IATA, @dummy, @dummy, @dummy, @dummy);
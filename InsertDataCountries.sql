use Airport;

LOAD DATA LOCAL INFILE "countries.csv"
  REPLACE
  INTO TABLE Country
  FIELDS
	OPTIONALLY ENCLOSED BY '"'
	TERMINATED BY ','
  LINES
	TERMINATED BY '\r\n'
  IGNORE 1 LINES
  (numCountry, codeCountry, Name, codeContinent, @wikiLink, @keyword);
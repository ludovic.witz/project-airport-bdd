use Airport;

SET FOREIGN_KEY_CHECKS=0;
DELETE FROM Distance;

DELIMITER $$


DROP PROCEDURE IF EXISTS InsertDataDistance$$

CREATE PROCEDURE InsertDataDistance() 
	BEGIN
	
		DECLARE Airport1Tmp 	VARCHAR(10);
		DECLARE Airport2Tmp 	VARCHAR(10);
		
		DECLARE Longitude1   FLOAT;
		DECLARE Latitude1    FLOAT;
		DECLARE Longitude2   FLOAT;
		DECLARE Latitude2    FLOAT;
		
		DECLARE done INT DEFAULT 0;
		DECLARE stop INT DEFAULT 0;
		
		DECLARE cursor1 CURSOR FOR SELECT AirportOACI, Latitude, Longitude FROM Airport;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
		
		/*uniquement pour strasbourg sinon beaucoup trop long*/
		SET Longitude1 = (SELECT Longitude FROM Airport WHERE AirportIATA = "SXB");
		SET Latitude1 = (SELECT Latitude FROM Airport WHERE AirportIATA = "SXB");
		SET Airport1Tmp = (SELECT AirportOACI FROM Airport WHERE AirportIATA = "SXB");
		
		
		/*utilisation de Fetch (attention très lent)*/
		OPEN cursor1;
		read_loop1: LOOP
			FETCH cursor1 INTO Airport2Tmp, Latitude2, Longitude2;
			IF done or stop > 2000 THEN
				LEAVE read_loop1;
			END IF;
			 
			SET done = 0;
			SET stop = stop + 1;
			
			INSERT INTO Distance (Airport1OACI, Airport2OACI, Distance)
			VALUES (Airport1Tmp, Airport2Tmp, ((Latitude1-Latitude2)*(Latitude1-Latitude2)+(Longitude1-Longitude2)*(Longitude1-Longitude2)));
			
		END LOOP;
		CLOSE cursor1;
		
END$$


DELIMITER ;

CALL InsertDataDistance();


/*		DECLARE Airport2 	VARCHAR(5);
		DECLARE Distance	INTEGER;
		
		DECLARE cursor1 CURSOR FOR SELECT AirportIATA, Latitude, Longitude FROM Airport;
		
		DECLARE Longitude1   FLOAT;
		DECLARE Latitude1    FLOAT;
		DECLARE Longitude2   FLOAT;
		DECLARE Latitude2    FLOAT;
		
		
		DECLARE done INT DEFAULT 0;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
		
		
		OPEN cursor1; 
		read_loop1: LOOP
			FETCH cursor1 INTO Airport1, Latitude1, Longitude1;
			IF done THEN
				LEAVE read_loop1;
			END IF;
			
			DECLARE cursor2 CURSOR FOR SELECT AirportIATA2, Latitude2, Longitude2 FROM Airport;
		   
			OPEN cursor2; 
			read_loop2: LOOP
			   FETCH cursor1 INTO Airport2, Latitude2, Longitude2;
			   IF done THEN
				  LEAVE read_loop2;
			   END IF;
			   
			   done = 0;
			   
			   INSERT INTO Distance (Airport1, Airport2, Distance)
					VALUES (Airport1, Airport2, 0)
				
			   
			   
			   
			END LOOP;
			CLOSE cursor2;
		   
		   
			SELECT * FROM table2 WHERE c1=arg0 AND c2=arg1;
		END LOOP;
		CLOSE cursor1;*/
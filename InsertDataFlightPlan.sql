use Airport;

LOAD DATA LOCAL INFILE "flightplan.csv"
  REPLACE
  INTO TABLE FlightPlan
  FIELDS
	OPTIONALLY ENCLOSED BY '"'
	TERMINATED BY ';'
  LINES
	TERMINATED BY '\r\n'
  IGNORE 1 LINES
  (numFlight,Airport,@Departure,@Arrival)
  SET
  Departure = case @Departure when '' then null else @Departure END,
  Arrival = case @Arrival when '' then null else @Arrival END
  ;
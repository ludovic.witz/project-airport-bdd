use Airport;

LOAD DATA LOCAL INFILE "runways.csv"
  REPLACE
  INTO TABLE Runway
  FIELDS
	OPTIONALLY ENCLOSED BY '"'
	TERMINATED BY ','
  LINES
	TERMINATED BY '\r\n'
  IGNORE 1 LINES
  (@id,@airport_ref,@airport_ident,@length_ft,@width_ft,@surface,@lighted,@closed,@le_ident,@le_latitude_deg,@le_longitude_deg,@le_elevation_ft,@le_heading_degT,@le_displaced_threshold_ft,@local_code,@he_ident,@he_latitude_deg,@he_longitude_deg,@he_elevation_ft,@he_heading_degT,@he_displaced_threshold_ft)
  SET
  numRunway = @id,
  Length = case @length_ft when '' then null else @length_ft END,
  LastCheckup = NOW() - INTERVAL FLOOR(RAND() * 14) DAY,
  Open = case @closed when 0 then 1 else 0 END,
  AirportOACI = @airport_ident,
  Surface = case @surface when '' then null else @surface END
  ;



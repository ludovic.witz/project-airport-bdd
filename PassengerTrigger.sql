delimiter //
DROP TRIGGER IF EXISTS bad_passenger;

CREATE TRIGGER bad_passenger BEFORE INSERT ON Passenger
	FOR EACH ROW
	BEGIN
		IF NEW.Name IS NOT NULL THEN
		
			IF NEW.Name = "gros_mot" THEN
	   
				SET NEW.Name = "bad_passenger";
			
			END IF;
	   
		END IF;
	END;//
delimiter ;
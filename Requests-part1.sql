/*On souhaite à partir d’un numéro d’avion connaître le prochain vol qui lui est affecté avec les différents aéroports où il peut faire escale.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetNextPlaneFlight;
CREATE PROCEDURE GetNextPlaneFlight(IN plane INT)      
BEGIN
    SELECT *
    FROM Flight join FlightPlan on Flight.numFlight=FlightPlan.numFlight
	where PlaneIATA=plane and Departure is not null and Departure > "2020-06-12" order by Departure limit 1;
END|

DELIMITER ;

Call GetNextPlaneFlight(310);

/*On souhaite connaître sur un mois les vols partants d’Entzheim ou faisant escale à Entzheim.*/
Select * from FlightPlan 
where Airport = 'SXB' and (Departure Between "2020-06-12" and ("2020-06-12" + INTERVAL 30 DAY) or Arrival Between "2020-06-12" and ("2020-06-12" + INTERVAL 30 DAY));

/*On souhaite connaître le nombre de départ par aéroport cette semaine.*/
Select count(*) as Number_of_departures, Airport from FlightPlan 
 where (Departure Between "2020-06-12" and ("2020-06-12" + INTERVAL 7 DAY)) group by Airport;

/*On souhaite, pour un vol donné, connaître l'état de toutes les pistes des aéroports.*/
DELIMITER |

DROP PROCEDURE IF EXISTS CanLand;
CREATE PROCEDURE CanLand(IN flight INT)
BEGIN
	SELECT Airport, Name, numRunway, Open FROM FlightPlan
	JOIN Airport ON FlightPlan.Airport = Airport.AirportIATA
	JOIN Runway ON Airport.AirportOACI = Runway.AirportOACI
	WHERE FlightPlan.numFlight = flight;
END|

DELIMITER ;

Call CanLand(5);

/*On souhaite connaitre les employés en charge d’un vol.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetEmployeFromFlight;
CREATE PROCEDURE GetEmployeFromFlight(IN flight INT)      
BEGIN
	SELECT Name, Flight.numFlight FROM Employe
	JOIN FlightEmploye ON FlightEmploye.numEmploye = Employe.numEmploye
	JOIN Flight ON Flight.numFlight = FlightEmploye.numFlight
	WHERE Flight.numFlight = flight;
END|

DELIMITER ;

Call GetEmployeFromFlight(5);

/*On souhaite connaître les positions des aéroports d’un vol pour configurer l’autopilote.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetCoordinatesAirport;
CREATE PROCEDURE GetCoordinatesAirport(IN flight INT)      
BEGIN
	SELECT Airport, Name, Latitude, Longitude, Altitude FROM FlightPlan
	JOIN Airport ON FlightPlan.Airport = Airport.AirportIATA
	WHERE numFlight = flight AND Arrival IS NOT NULL;
END|

DELIMITER ;

Call GetCoordinatesAirport(373);

/*On veut afficher tous les départs d’un aéroport donné.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetDepartureFromAirport;
CREATE PROCEDURE GetDepartureFromAirport(IN airport CHAR(3))      
BEGIN
	SELECT * FROM FlightPlan
	WHERE FlightPlan.Airport = airport AND FlightPlan.Departure >= "2020-06-12";
END|

DELIMITER ;

Call GetDepartureFromAirport('SXB');

/*On souhaite connaitre le trajet d’un passager.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetPassengerFlight;
CREATE PROCEDURE GetPassengerFlight(IN passengerName VARCHAR(50))      
BEGIN
	SELECT Airport, Departure, Arrival, Name FROM FlightPlan
	JOIN Passenger ON FlightPlan.numFlight = Passenger.numFlight
	WHERE Passenger.Name = passengerName;
END|

DELIMITER ;

Call GetPassengerFlight('Chase Wallace');






/*On souhaite connaître l'aéroport qui a le plus de départ et le moins de départ. Idem pour les arrivés.*/
SELECT Name, Count(*) as Number FROM FlightPlan 
JOIN Airport ON Airport.AirportIATA = FlightPlan.Airport
WHERE Departure IS NOT NULL 
GROUP BY Airport
ORDER BY Number DESC
LIMIT 1;


SELECT Name, Count(*) as Number FROM FlightPlan 
JOIN Airport ON Airport.AirportIATA = FlightPlan.Airport
WHERE Departure IS NOT NULL 
GROUP BY Airport
ORDER BY Number ASC
LIMIT 1;

SELECT Name, Count(*) as Number FROM FlightPlan 
JOIN Airport ON Airport.AirportIATA = FlightPlan.Airport
WHERE Arrival IS NOT NULL 
GROUP BY Airport
ORDER BY Number DESC
LIMIT 1;


SELECT Name, Count(*) as Number FROM FlightPlan 
JOIN Airport ON Airport.AirportIATA = FlightPlan.Airport
WHERE Arrival IS NOT NULL 
GROUP BY Airport
ORDER BY Number ASC
LIMIT 1;

/*On veut connaître la distance parcourue par un avion par vol.*/

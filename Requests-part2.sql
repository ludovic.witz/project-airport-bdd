/*Script de requetes*/
Use Airport;

/* k) On veut planifier les vols d’un nouvel avion qui nécessite une piste de 3km pour atterrir, quels aéroports sont capable de l’accueillir ?*/
SELECT Name, Length FROM Airport
JOIN Runway ON Runway.AirportOACI = Airport.AirportOACI
WHERE Runway.Length >= 18045 AND (Runway.Surface LIKE "%asp" OR Runway.Surface LIKE "%con%");

/* l) On veut la liste des passagers du vol 18*/
SELECT Name FROM Passenger
JOIN Flight ON Flight.numFlight = Passenger.numFlight
WHERE Flight.numFlight = 18;

/* m) On veut savoir quelle compagnie propose un certain type d'avions.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetCompanyFromPlaneType;
CREATE PROCEDURE GetCompanyFromPlaneType(IN type CHAR(3))      
BEGIN
	SELECT Name FROM Company
	JOIN Flight ON Flight.numCompany = Company.numCompany
	WHERE Flight.PlaneIATA = type;
END|

DELIMITER ;

Call GetCompanyFromPlaneType(310);

/* n) On souhaite changer à la dernière minute d’aéroport d'arrivé, trouver l’aéroport le plus proche de celui initialement prévu.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetCloserAirport;
CREATE PROCEDURE GetCloserAirport(IN airport CHAR(3))      
BEGIN
	SET @LongitudeInitial = (SELECT Longitude FROM Airport WHERE AirportIATA = airport);
	SET @LatitudeInitial = (SELECT Latitude FROM Airport WHERE AirportIATA = airport);

	SELECT AirportIATA, Name, ((Longitude - @LongitudeInitial) * (Longitude - @LongitudeInitial)) + ((Latitude - @LatitudeInitial) * (Latitude - @LatitudeInitial)) as Distance FROM Airport
	WHERE AirportIATA != airport AND AirportIATA != ''
	ORDER BY Distance ASC
	LIMIT 1;
END|

DELIMITER ;

Call GetCloserAirport('BSL');

/* o) On souhaite connaître le temps de vol pour un vol donné.*/
DELIMITER |

DROP PROCEDURE IF EXISTS GetFlightTime;
CREATE PROCEDURE GetFlightTime(IN flight INT)      
BEGIN
	SET @Departure = (SELECT Departure FROM FlightPlan WHERE FlightPlan.numFlight = flight AND Arrival IS NULL);
	SET @Arrival = (SELECT Arrival FROM FlightPlan WHERE FlightPlan.numFlight = flight AND Departure IS NULL);
	SELECT TIMEDIFF(@Arrival, @Departure) as FlightTime;
END|

DELIMITER ;

Call GetFlightTime(5);

/* p) On souhaite connaître les pistes qui nécessitent un entretien.*/
SELECT numRunway, Name, LastCheckup FROM Airport
JOIN Runway ON Runway.AirportOACI = Airport.AirportOACI
WHERE Runway.LastCheckup <= "2020-05-31 11:30:00"
LIMIT 100;

/* q) On souhaite connaître les avions qui ont un vol prévu dans les prochaines 24h.*/
SELECT * FROM FlightPlan
JOIN Flight ON Flight.numFlight = FlightPlan.numFlight
WHERE FlightPlan.Departure BETWEEN "2020-06-12" AND "2020-06-12" + INTERVAL 24 HOUR;

/* r) On souhaite connaître le nombre d'aéroport par pays.*/
SELECT Country.Name as CountryName, Count(*) as NumberOfAirport FROM Airport
JOIN Country ON Country.codeCountry = Airport.codeCountry
GROUP BY Country.Name
ORDER BY Country.Name;

/* s) On souhaite connaître le type d’avion le plus populaire.*/
SELECT Maker, Model, Max(Number) as Number FROM (
SELECT Maker, Model, Count(*) as Number FROM Plane
JOIN Flight ON Flight.PlaneIATA = Plane.IATA
GROUP BY IATA) as NbPlanes;
source CreateDB.sql
source CreateTables.sql

source PassengerTrigger.sql

source InsertDataContinent.sql
source InsertDataCountries.sql
source InsertDataAirport.sql
source InsertDataEmploye.sql
source InsertDataRunway.sql
source InsertDataCompany.sql
source InsertDataPlane.sql
source InsertDataFlight.sql
source InsertDataPassenger.sql
source InsertDataFlightPlan.sql
source InsertDataFlightEmploye.sql
source InsertDataDistance.sql